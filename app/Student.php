<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    // by default laravel will connetc table with (es/s) from model name
    // protected $table = 'students';

    // make fillable table from database for secure
    protected $fillable = ['name','address','postcode','telephone'];
}
