<?php

namespace App\Http\Controllers;

use App\Student; // model name
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::all();
        // return view('students.index', ['student' => $students]);
        
        // penulisan code bisa tipersingkat apabila namanya sama
        // contoh dibawah ini
        return view('students.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // test value
        // return $request;

        // general code for create database
        // $student = new Student;
        // $student->name = $request->name;
        // $student->address = $request->address;
        // $student->postcode = $request->postcode;
        // $student->telephone = $request->telephone;

        // $student->save();

        // method crete from model laravel
        // Student::create([
        //     //array associative
        //     'name' => $request->name,
        //     'address' => $request->address,
        //     'postcode' => $request->postcode,
        //     'telephone' => $request->telephone
        // ]);

        // Form falidation
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'postcode' => 'required',
            'telephone' => 'required'
        ]);

        // method create can request all data if fillable model created
        Student::create($request->all());

        return redirect('/students')->with('status', 'Student Data has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return view('students.show',compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('students.edit' ,compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        // Form falidation
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'postcode' => 'required',
            'telephone' => 'required'
        ]);
        
        Student::where('id', $student->id)
                ->update([
                    'name' => $request->name,
                    'address' => $request->address,
                    'postcode' => $request->postcode,
                    'telephone' => $request->telephone
                ]);
        return redirect('/students')->with('status', 'Student Data has been Update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        Student::destroy($student->id);
        return redirect('/students')->with('status', 'Student Data has been delete!');
    }
}