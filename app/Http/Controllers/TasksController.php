<?php

namespace App\Http\Controllers;

use App\Task; // model name
use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = task::all();
        // return view('tasks.index', ['task' => $tasks]);
        
        // penulisan code bisa tipersingkat apabila namanya sama
        // contoh dibawah ini
        return view('tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $temp = explode(' ',$data['input']);
        // print_r($temp);
        if (($key = array_search("Tel", $temp)) !== false) {
            // print("1");
            unset($temp[$key]);
        }
        if (($key = array_search("Post", $temp)) !== false) {
            unset($temp[$key]);
            // print("2");
        }
        if (($key = array_search("Code", $temp)) !== false) {
            unset($temp[$key]);
            // print("3");
        }

        $tel = end($temp);
        if (($key = array_search($tel, $temp)) !== false) {
            unset($temp[$key]);
        }
        $name="";
        $address="";
        foreach($temp as $values){
            if(!preg_match('/([a-z0-9])/', $values)){
                if (($key = array_search($values, $temp)) !== false) {
                    unset($temp[$key]);
                }
                    $name.=$values." ";
            }
            if((strlen($values)==5) && (!preg_match('/([A-Za-z])/', $values))){
                if (($key = array_search($values, $temp)) !== false) {
                    unset($temp[$key]);
                }
                $postcode=$values;
            }
            
        }
        foreach($temp as $values){
            $address.=$values." ";
        }

        print($name);
        print('<br>');
        print($address);
        print('<br>');
        print($postcode);
        print('<br>');
        print($tel);
        print('<br>');
        

        // method crete from model laravel
        Task::create([
            //array associative
            'name' => $name,
            'address' => $address,
            'postcode' => $postcode,
            'telephone' => $tel
        ]);

        // method create can request all data if fillable model created
        // Task::create($request->all());

        return redirect('/tasks')->with('status', "\nName\t: ".$name."\n | Address\t: ".$address."\n | Post Code\t: ".$postcode."\n | Telephone\t: ".$tel);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(task $task)
    {
        return view('tasks.show',compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(task $task)
    {
        return view('tasks.edit' ,compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, task $task)
    {
        // Form falidation
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'postcode' => 'required',
            'telephone' => 'required'
        ]);
        
        task::where('id', $task->id)
                ->update([
                    'name' => $request->name,
                    'address' => $request->address,
                    'postcode' => $request->postcode,
                    'telephone' => $request->telephone
                ]);
        return redirect('/tasks')->with('status', 'Task Data has been Update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(task $task)
    {
        task::destroy($task->id);
        return redirect('/tasks')->with('status', 'Task Data has been delete!');
    }
}