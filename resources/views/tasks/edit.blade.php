@extends('layout/main') 

@section('title', 'Form Edit Tasks')

@section('container') 
<div class = "container">
    <div class="row">
        <div class="col-8">
            <h1 class="mt-3">Form Edit Tasks
            </h1>
            <form method="post" action="/tasks/{{ $task->id }}">
                {{-- @method('patch') or @method('put') is a same --}}
                @method('put')
                @csrf
                <div class="form-group">
                    <label for="Name">Name</label>
                    <input
                        type="text"
                        class="form-control @error('name') is-invalid @enderror"
                        id="nsme"
                        placeholder="input name"
                        name="name"
                        value="{{ $task->name }}">@error('name')<div class="invalid-feedback">{{ $message }}</div>@enderror</div>
                <div class="form-group">
                    <label for="Address">Address</label>
                    <input
                        type="text"
                        class="form-control @error('address') is-invalid @enderror"
                        id="nsme"
                        placeholder="input address"
                        name="address"
                        value="{{ $task->address }}">@error('address')<div class="invalid-feedback">{{ $message }}</div>@enderror</div>
                <div class="form-group">
                    <label for="Postcode">Post code</label>
                    <input
                        type="text"
                        class="form-control @error('postcode') is-invalid @enderror"
                        id="nsme"
                        placeholder="input postcode"
                        name="postcode"
                        value="{{ $task->postcode }}">@error('postcode')<div class="invalid-feedback">{{ $message }}</div>@enderror</div>
                <div class="form-group">
                    <label for="Telephone">Telephone</label>
                    <input
                        type="text"
                        class="form-control @error('telephone') is-invalid @enderror"
                        id="nsme"
                        placeholder="input telephone"
                        name="telephone"
                        value="{{ $task->telephone }}">@error('telephone')<div class="invalid-feedback">{{ $message }}</div>@enderror</div>
                <button type="submit" class="btn btn-primary">Update task!</button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection