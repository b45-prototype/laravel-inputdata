@extends('layout/main')

@section('title','Detail Tasks')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-6">
                <h1 class="mt-3">Detail Tasks </h1>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Name      : {{$task->name}}</h5>
                        <h5 class="card-text">Address   : {{$task->address}}</h5>
                        <h5 class="card-text">Post Code : {{$task->postcode}}</h5>
                        <h5 class="card-text">Telephone : {{$task->telephone}}</h5>
                        <a href="{{ $task->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/tasks/{{ $task->id }}" method="post" class="d-inline">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                        <a href="/tasks" class="card-link">Back</a>
                    </div>
                </div>            
            </div>
        </div>
    </div>
@endsection 