@extends('layout/main')

@section('title','Tasks')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-3">List Task </h1>
                <a href="/tasks/create" class="btn btn-primary my-3">Add data task</a>

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status')}}
                    </div>
                @endif

                <ul class="list-group">
                @foreach($tasks as $task)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        {{$task->name}}
                        <a href="/tasks/{{$task->id}}" class="badge badge-info">detail</a>
                    </li>
                @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection 