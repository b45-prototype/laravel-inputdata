@extends('layout/main') 

@section('title', 'Form Add Tasks')

@section('container') 
<div class = "container">
    <div class="row">
        <div class="col-8">
            <h1 class="mt-3">Form Add Tasks
            </h1>
            <form method="post" action="/tasks">
                @csrf
                <div class="form-group">
                    <label for="Input">Input</label>
                    <input
                        type="text"
                        class="form-control @error('input') is-invalid @enderror"
                        id="nsme"
                        placeholder="input text"
                        name="input"
                        value="{{old('input')}}">@error('input')<div class="invalid-feedback">{{ $message }}</div>@enderror</div>
                <button type="submit" class="btn btn-primary">OK</button>
            </form>
        </div>
    </div>
</div>
</div>
@endsection