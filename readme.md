# Framework : Laravel
# Database  : MYSQL

# Required composer
# Don't forget to import database, migrate and config
    - file database on directory database laravel_inputdata.sql
    - migrate database
    - config database on root directory file .env

# laravel-inputdata has been 3 main menu

    # Home
        Empty Home Page
        
    # Task
    
        - create 1 page, show 1 input text field and 1 button OK
        - fill input with raw data (sample below)
        - when click "OK", then parse the raw data into name, address, post code, and telephone
        - show results in this page
        - also save the result into database

        Sample I:
        
        PRABOWO Kerto Raharjo No.1 Malang East Java Indonesia Post Code 65142 Tel +62341-333444
        
        OK
        
        When OK button is clicked then：
        
        Result：
        NAME        ： PRABOWO
        ADDRESS     ： Kerto Raharjo No.1 Malang East Java Indonesia
        POST CODE   ： 65142
        TELEPHONE   ： +62341-333444
        
        
        Sample II:
        
        JOKO WIDODO Jl. Joyo Agung No.13 Merjosari Malang 65142 0341-333444
        
        OK
        
        When OK button is clicked then：
        
        Result：
        NAME        ：  JOKO WIDODO
        ADDRESS     ： Jl. Joyo Agung No.13 Merjosari Malang
        POST CODE   ： 65142
        TELEPHONE   ： 0341-333444
        
    # Students
    
        CRUD Laravel with bootstrap